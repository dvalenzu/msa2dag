#!/usr/bin/env bash
set -o errexit
set -o nounset

#TOOL=""
#TOOL="gdb --args"
TOOL="valgrind --vgdb-error=1 --leak-check=full --show-leak-kinds=all"

for FILE in example_data/*txt; do
	echo ${FILE}
	${TOOL} ./multi_test ${FILE}
done
