#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <assert.h>
#include <stdlib.h> 

#include <msa2dag/graph_utils.h>
#include <msa2dag/graph.h>

using MSA2DAG::Graph;
using MSA2DAG::matrix_t;
using namespace std;

/*
void testMerge(matrix_t M) {
	Graph master_graph;
	master_graph.BuildFromMatrix(M);
	master_graph.PrintSomeInfo();
	master_graph.PrintToDot("tmp.masterplot.gv");
	
	size_t n_cols = M[0].size();
	cerr << "N cols: " << n_cols << endl;
	string basename = "tmp.split";
	for (size_t limit = 1; limit < n_cols -1; limit ++) {
		cerr << "Limit: " << limit << endl;
		cerr << "splitting: 0 - " << limit << " + " << limit + 1 << " - " << n_cols - 1 << endl;
		Graph g1, g2;
		g1.BuildFromMatrixRange(M, 0, limit);
		g2.BuildFromMatrixRange(M, limit+1, n_cols - 1);
		cerr << "G1:" << endl;
		g1.PrintSomeInfo();
		cerr << "G2:" << endl;
		g2.PrintSomeInfo();
		
		string f1 =  basename+to_string(limit)+string("_part1.gv");
		string f2 =  basename+to_string(limit)+string("_part2.gv");
		
		g1.PrintToDot(f1);
		g2.PrintToDot(f2);

		g1.SpecialMerge(g2);
		string ff =  basename+to_string(limit)+"_all.gv";
		g1.PrintToDot(ff);
		master_graph.TestEquallity(g1);
	}
}
*/

void testIncrementalBuild_v1(matrix_t M) {
	Graph master_graph;
	master_graph.BuildFromMatrix(M);
	master_graph.PrintSomeInfo();
	master_graph.PrintToDot("tmp.masterplot.gv");
	
	size_t n_cols = M[0].size();
	cerr << "N cols: " << n_cols << endl;
	string basename = "tmp.split";
	for (size_t limit = 1; limit < n_cols -1; limit ++) {
		cerr << "Limit: " << limit << endl;
		cerr << "splitting: 0 - " << limit << " + " << limit + 1 << " - " << n_cols - 1 << endl;
		
		Graph inc_graph;
		
		inc_graph.Init(M.size());
		
		inc_graph.AppendMatrixRange(M, 0, limit);
		inc_graph.AppendMatrixRange(M, limit+1, n_cols - 1);
		inc_graph.CompleteConstruction();

		master_graph.TestEquallity(inc_graph);
	}

}

void testIncrementalBuild_v2(matrix_t M) {
	Graph master_graph;
	master_graph.BuildFromMatrix(M);
	master_graph.PrintSomeInfo();
	master_graph.PrintToDot("tmp.masterplot.gv");
	
	size_t n_cols = M[0].size();
	cerr << "N cols: " << n_cols << endl;
	string basename = "tmp.split";

	Graph inc_graph;
	inc_graph.Init(M.size());
	for (size_t i = 0; i < n_cols; i++) {
		inc_graph.AppendMatrixRange(M, i, i);
	}
	inc_graph.CompleteConstruction();
	master_graph.TestEquallity(inc_graph);
}

int main(int argc, char *argv[]) {
  if (argc < 2 ) {
    cerr << "Usage: " << endl;
    cerr << " " << argv[0] << " <source files>" << endl;
    exit(1);
  }
	
	matrix_t M;
	M = MSA2DAG::GraphUtils::ReadMatrixFromFiles(argc-1, argv+1);
	
	//testMerge(M);
	testIncrementalBuild_v1(M);


	cerr << "Success." << endl;
}
