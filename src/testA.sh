#!/usr/bin/env bash
set -o errexit
set -o nounset

TOOL=""
#TOOL="gdb --args"
#TOOL="valgrind --vgdb-error=1 --leak-check=full --show-leak-kinds=all"

${TOOL} ./msa2dag ./example_data/A.txt > test_graph_A.dot
cat test_graph_A.dot | dot -Tpdf -o test_graph_A.pdf
