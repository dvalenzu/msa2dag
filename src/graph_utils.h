#ifndef GRAPH_UTILS_HPP_ 
#define GRAPH_UTILS_HPP_ 

#include <vector>
#include <msa2dag/graph.h>

namespace MSA2DAG { namespace GraphUtils {
	void MergeGraphs(std::vector<Graph> &graphs);
	Graph ParallelBuildFromMatrix(matrix_t const &M, size_t n_threads);
	bool EqualVectors(std::vector<size_t> const &A, std::vector<size_t> const &B);
	std::vector<std::string> ReadMatrixFromSingleFile(char const * filename);
	std::vector<std::vector<uint8_t>> ReadMatrixFromFiles(int n, char const * const filenames[]);
}}
#endif /* GRAPH_UTILS_HPP_*/
