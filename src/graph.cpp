#include <algorithm>
#include <iostream>
#include <fstream>
#include <map>
#include "graph.h"
#include "graph_utils.h"
#include "internal.h"

using namespace std;

namespace MSA2DAG { 

Graph::Graph() {
	n_nodes = 0;
}

void Graph::ValidateMatrix(matrix_t const &M) {
	ASSERT(M.size() == n_rows);
	for (size_t i = 1; i < n_rows; i++) {
		ASSERT(M[i].size() == M[0].size());
	}
}

void Graph::TestEquallity(Graph const &G) {
	ASSERT(outgoing.size() == n_nodes);
	//ASSERT(incoming.size() == n_nodes);
	ASSERT(labels.size() == n_nodes);

	ASSERT(n_nodes == G.n_nodes);
	ASSERT(outgoing.size() == G.outgoing.size());
	//ASSERT(incoming.size() == G.incoming.size());
	ASSERT(labels.size() == G.labels.size());

	for (size_t i = 0; i < n_nodes; i++) {
		ASSERT(labels[i] == G.labels[i]);
		//ASSERT(GraphUtils::EqualVectors(incoming[i], G.incoming[i]));
		ASSERT(GraphUtils::EqualVectors(outgoing[i], G.outgoing[i]));
	}

}

void Graph::BuildFromMatrix(matrix_t const &M) {
	//ValidateMatrix(M);
	Init(M.size());
	size_t cols = M[0].size();
	ASSERT(cols);
	AppendMatrixRange(M, 0, cols - 1);
	CompleteConstruction();
}

void Graph::BuildFromMatrixRange(matrix_t const &M, size_t first_col, size_t last_col) {
	Init(M.size());
	AppendMatrixRange(M, first_col, last_col);
	CompleteConstruction();
}

void Graph::Init(size_t _n_rows) {
	id_s = AddNode('$');  // S
	n_rows = _n_rows;
	front_wave = vector<size_t>(n_rows, id_s);
	back_wave = vector<size_t>(n_rows, id_s);
}

void Graph::AppendMatrixRange(matrix_t const &M) {
	size_t n_cols = M[0].size();
	ASSERT(n_cols);
	AppendMatrixRange(M, 0, n_cols-1);
}

void Graph::AppendMatrixRange(matrix_t const &M, size_t first_col, size_t last_col) {
	//ValidateMatrix(M);
	ASSERT(n_rows == M.size());

	for (size_t j = first_col; j <= last_col; j++) {
		std::map<char,size_t> new_nodes;

		for (size_t i = 0; i < n_rows; i++) {
			char next_label = (char)M[i][j];
			if (next_label == '-') continue;
			size_t next_id;
			if (new_nodes.count(next_label)) {
				next_id = new_nodes[next_label];
			} else {
				next_id = AddNode(next_label);
				new_nodes[next_label] = next_id;
			}

			if ( !HasNeighborByID(front_wave[i], next_id) ) {
				AddEdge(front_wave[i], next_id);
			}
			if (front_wave[i] == id_s) back_wave[i] = next_id;
			front_wave[i] = next_id;
		}
	}
}

void Graph::CompleteConstruction() {
	char final_label = '#';
	size_t final_id = AddNode(final_label);
	for (size_t i = 0; i < n_rows; i++) {
		if (!HasNeighborByID(front_wave[i], final_id))
			AddEdge(front_wave[i], final_id);
	}
	for (size_t i = 0; i < n_rows; i++) {
		if (back_wave[i] == id_s) back_wave[i] = final_id;
	}
	
	//SquashUnaryPaths();
	//NotSquash();
}

size_t Graph::AddNode(char label) {
	ASSERT(labels.size() == n_nodes);
	ASSERT(outgoing.size() == n_nodes);
	labels.push_back(label);
	outgoing.push_back(vector<size_t>(0));
	//incoming.push_back(vector<size_t>(0));
	n_nodes++;
	return n_nodes-1;
}

bool Graph::HasNeighborByID(size_t id_src, size_t id_target) {
	for (size_t i = 0; i < outgoing[id_src].size(); i++) {
		if (outgoing[id_src][i] == id_target) 
			return true;
	}
	return false;	
}

void Graph::AddEdge(size_t id_src, size_t id_target) {
#ifndef NDEBUG
	for (size_t i = 0; i < outgoing[id_src].size(); i++) {
		ASSERT(outgoing[id_src][i] != id_target);
	}
	//for (size_t i = 0; i < incoming[id_target].size(); i++) {
	//	ASSERT(incoming[id_target][i] != id_src);
	//}
#endif
	outgoing[id_src].push_back(id_target);
	//incoming[id_target].push_back(id_src);
}

void Graph::AddEdgeSafely(size_t id_src, size_t id_target) {
	for (size_t i = 0; i < outgoing[id_src].size(); i++) {
		if (outgoing[id_src][i] == id_target) return;
	}
	AddEdge(id_src, id_target);
}

bool Graph::HasOneOutgoing(size_t id) {
	return (outgoing[id].size() == 1);
}

/*
bool Graph::HasOneIncoming(size_t id) {
	return (incoming[id].size() == 1);
}
*/

void Graph::ValidateFirstWave(vector<size_t> const &wave) {
	for (size_t i = 0; i < wave.size(); i++) {
		if (wave[i] == 0) {
			cerr << "Fatal error:" << endl;
			cerr << "There is a first wave set to S" << endl;
			cerr << "Meaning that there is a row full of gaps in a chunk, thus it cannot be merged" << endl;
			exit(33);
		}
	}
}

/*
void Graph::SpecialMerge(Graph const &G) {
	vector<size_t> const &new_wave = G.back_wave;
	auto new_incoming = G.incoming;
	// Think: maybe it is ok, as the S and T will "link" properly
	//ValidateFirstWave(new_wave);  //validate last too ?

	size_t new_n_nodes = n_nodes + G.n_nodes - 2;
	outgoing.resize(new_n_nodes);
	incoming.resize(new_n_nodes);
	labels.resize(new_n_nodes);

	// remove links as part of the merge:
	{
		std::vector<size_t> fw_set(front_wave);
		std::sort(fw_set.begin(), fw_set.end());
		auto last_fw = std::unique(fw_set.begin(), fw_set.end());
		fw_set.erase(last_fw, fw_set.end());
		for (auto id : fw_set) {
			size_t prev_size = outgoing[id].size();
			for (size_t pos = 0; pos < outgoing[id].size(); pos ++) {
				if (outgoing[id][pos] == n_nodes - 1) {
					outgoing[id].erase(outgoing[id].begin()+ (long)pos);
				}
			}
			ASSERT(outgoing[id].size() == prev_size - 1);
		}
	}

	{
		std::vector<size_t> bw_set(G.back_wave);
		std::sort(bw_set.begin(), bw_set.end());
		auto last_bw = std::unique(bw_set.begin(), bw_set.end());
		bw_set.erase(last_bw, bw_set.end());
		for (auto id : bw_set) {
			size_t prev_size = new_incoming[id].size();
			for (size_t pos = 0; pos < new_incoming[id].size(); pos ++) {
				if (new_incoming[id][pos] == 0) {
					new_incoming[id].erase(new_incoming[id].begin()+ (long)pos);
				}
			}
			// UNLESS, there is a dash-only raw. 
			// In that case, S is in G.back_wave, but it dows not have predessors.
			ASSERT(new_incoming[id].size() == prev_size - 1 || prev_size == 0);
		}
	}

	// merge nodes and relabel them (and edges) accordingly
	// ignoring special nodes: this.T  and other.S
	size_t offset = n_nodes - 2;
	for (size_t i = 1; i < G.n_nodes; i++) {
		outgoing[offset + i] = G.outgoing[i];
		incoming[offset + i] = new_incoming[i];
		for (size_t j = 0; j < outgoing[offset+i].size(); j++) {
			outgoing[offset+i][j] += offset;	
		}
		for (size_t j = 0; j < incoming[offset+i].size(); j++) {
			incoming[offset+i][j] += offset;	
		}
		labels[offset + i] = G.labels[i];
	}

	ASSERT(front_wave.size() == new_wave.size());

	for (size_t i = 0; i < front_wave.size(); i++) {
		size_t source_id = front_wave[i];
		size_t target_id = new_wave[i] + offset;
		AddEdgeSafely(source_id, target_id);
	}
	front_wave = G.front_wave;
	for (size_t i = 0; i < front_wave.size(); i++) {
		front_wave[i] += offset;
	}
	n_nodes = new_n_nodes;
}
*/

// this is leaving some inconsistency, probably Y should resize the vectors accordingly.a
/*
void Graph::SquashUnaryPaths() {
	to_skip = vector<bool>(n_nodes, false);
	long_labels = vector<string> (n_nodes);
	for (size_t i = 1; i < n_nodes - 1; i++) {
		if (to_skip[i]) continue;
		ASSERT(outgoing[i].size() > 0);
		size_t curr = i;
		size_t next = outgoing[i][0];
		string new_label;
		new_label.push_back(labels[i]);
		while (HasOneOutgoing(curr) && HasOneIncoming(next) && next < n_nodes - 1) {
			new_label.push_back(labels[next]);
			to_skip[next] = true;
			curr = next;
			next = outgoing[next][0];
		}
		long_labels[i] = new_label;
		outgoing[i] = outgoing[curr];
	}
	long_labels[0] = string(1,labels[0]);
	long_labels[n_nodes-1] = string(1,labels[n_nodes-1]);
}
*/
/*
void Graph::NotSquash() {
	long_labels = vector<string> (n_nodes);
	to_skip = vector<bool>(n_nodes, false);
	for (size_t i = 0; i < n_nodes; i++) {
		long_labels[i] = string(1,labels[i]);
	}
}
*/
void Graph::PrintSomeInfo() {
	cerr << "---------------" << endl;
	cerr << "Back Wave:" << endl;
	cerr << "---------------" << endl;
	for (size_t k = 0; k < back_wave.size(); k ++) {
		size_t i = back_wave[k];
		cerr <<  "\t" << i << "  [label=\"" << i << ":"<< labels[i] << "\"];" << endl;
	}
	cerr << "---------------" << endl;
	cerr << endl;
	cerr << "---------------" << endl;
	cerr << "Front Wave:" << endl;
	cerr << "---------------" << endl;
	for (size_t k = 0; k < front_wave.size(); k ++) {
		size_t i = front_wave[k];
		cerr <<  "\t" << i << "  [label=\"" << i << ":"<< labels[i] << "\"];" << endl;
	}
	cerr << "---------------" << endl;
	cerr << endl;
}

void Graph::PrintToGFA (string const &filename) {
	std::ofstream myout;
	myout.open(filename);
	//SquashUnaryPaths();
	//NotSquash();

	PrintToGFA(myout);
	
	myout.close();
}

void Graph::PrintToGFA (std::ostream &myout) {
	myout << "H\tVN:Z:1.0" << endl;
	for (size_t i  = 0; i < n_nodes; i++) {
		myout <<  "S\t" << (i+1) << "\t" << labels[i] << endl;
		if (outgoing[i].size()) {
			for (size_t j = 0; j < outgoing[i].size(); j++) {
				myout <<  "L\t" << (i+1) << "\t+\t" << (outgoing[i][j]+1) << "\t+\t0M" << endl;
			}
		}
	}
}

void Graph::PrintToDot (string const &filename) {
	std::ofstream myout;
	myout.open(filename);
	//SquashUnaryPaths();
	//NotSquash();
	myout << "digraph {" << endl;
	myout << "\trankdir=LR; // Left to Right, instead of Top to Bottom" << endl;

	for (size_t i  = 0; i < n_nodes; i++) {
		//if (to_skip[i]) cerr << "not printing " << i << endl;
		//if (to_skip[i]) continue;
		if (outgoing[i].size()) {
			myout << "\t" << i << " -> { ";
			for (size_t j = 0; j < outgoing[i].size(); j++) {
				myout << outgoing[i][j] << " ";
			}
			myout << " };" << endl;
		}
		//myout <<  "\t" << i << "  [label=\"" << i << ":"<< long_labels[i] << "\"];" << endl;
		myout <<  "\t" << i << "  [label=\"" << i << ":"<< labels[i] << "\"];" << endl;
	}
	myout << "}" << endl;
	myout.close();
}

void Graph::PrintToDot (ostream &os) {
	os << "digraph {" << endl;
	os << "\trankdir=LR; // Left to Right, instead of Top to Bottom" << endl;

	for (size_t i  = 0; i < n_nodes; i++) {
		//if (to_skip[i]) continue;
		if (outgoing[i].size()) {
			os << "\t" << i << " -> { ";
			for (size_t j = 0; j < outgoing[i].size(); j++) {
				os << outgoing[i][j] << " ";
			}
			os << " };" << endl;
		}
		//os <<  "\t" << i << "  [label=\"" << i << ":"<< long_labels[i] << "\"];" << endl;
		os <<  "\t" << i << "  [label=\"" << i << ":"<< labels[i] << "\"];" << endl;
	}
	os << "}" << endl;
}
}
