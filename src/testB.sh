#!/usr/bin/env bash
set -o errexit
set -o nounset

TOOL=""
#TOOL="gdb --args"
#TOOL="valgrind --vgdb-error=1 --leak-check=full --show-leak-kinds=all"

${TOOL} ./msa2dag ./example_data/files_tmp/* > tmp_graph_multi.dot
cat tmp_graph_multi.dot | dot -Tpdf -o tmp_graph_multi.pdf
