#ifndef SRC_COMMON_H_
#define SRC_COMMON_H_

#include <cstdint>
#include <vector>

namespace MSA2DAG {

	//typedef vector<string> matrix_t;
	typedef std::vector<std::vector<std::uint8_t>> matrix_t;
}
#endif  // SRC_COMMON_H_
