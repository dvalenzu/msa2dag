#ifndef SRC_INTERNAL_H_
#define SRC_INTERNAL_H_

#ifndef uchar
#define uchar unsigned char
#endif

// This is a trick to avoid warnings for unused variables that are used only for assert:
#ifdef NDEBUG
#define ASSERT(x) do { (void)sizeof(x);} while (0)
#else
#include <assert.h>
#define ASSERT(x) assert(x)
#endif

#endif  // SRC_INTERNAL_H_
