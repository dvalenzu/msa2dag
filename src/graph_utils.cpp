#include <fstream>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <assert.h>
#include <stdlib.h> 
#include <algorithm>

#include "graph.h"
#include "graph_utils.h"
#include "internal.h"

using namespace std;


namespace MSA2DAG { namespace GraphUtils {
/*
	void MergeGraphs(vector<MSA2DAG::Graph> &graphs) {
		// given the zero-node deletion, this is not so terrible.
		// a different represenation of S and T could allow for a faster merge.
		for (size_t i = 1; i < graphs.size(); i++) {
			graphs[0].SpecialMerge(graphs[i]);	
		}
	}

	MSA2DAG::Graph ParallelBuildFromMatrix(matrix_t const &M, size_t n_threads) {
		//validateMatrix(M);
		size_t cols = M[0].size();
		vector<MSA2DAG::Graph> graphs(n_threads);
		for (size_t i = 0; i < n_threads; i++) {
			size_t sp = i*(cols/n_threads);
			size_t ep = (i+1)*(cols/n_threads) - 1;
			if (i == n_threads - 1) ep = cols - 1;
			cerr << sp << "," << ep << endl;
			graphs[i].BuildFromMatrixRange(M, sp,ep);
		}
		MergeGraphs(graphs);
		return graphs[0];
	}
	*/

	bool EqualVectors(vector<size_t> const &A, vector<size_t> const &B) {
		return is_permutation (A.begin(), A.end(), B.begin());
	}

	vector<string> ReadMatrixFromSingleFile(char const * filename) {
		size_t cols = 0;
		std::ifstream infile(filename);
		std::string line;
		vector<string> matrix;
		while (std::getline(infile, line)) {
			ASSERT (cols == 0 || cols == line.size() );
			cols = line.size();
			matrix.push_back(line);
		}
		infile.close();
		return matrix;
	}

	vector<vector<uint8_t>> ReadMatrixFromFiles(int n, char const * const filenames[]) {
		vector<vector<uint8_t>> matrix;
		size_t cols = 0;
		for (int i = 0; i < n; i++) {
			cerr << "Reading:" << filenames[i] << endl;
			std::ifstream infile(filenames[i]);
			std::string line;
			vector<uint8_t> formatted;
			while (std::getline(infile, line)) {
				ASSERT (cols == 0 || cols == line.size() );
				cols = line.size();
				formatted.resize(cols);
				for(size_t j = 0; j < cols; j++) formatted[j] = (uint8_t)line[j];
				matrix.push_back(formatted);
			}
			infile.close();
		}
		return matrix;
	}
}}
