#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <assert.h>
#include <stdlib.h> 

#include "common.h"
#include "graph.h"
#include "graph_utils.h"
#include <getopt.h>

using namespace std;

using MSA2DAG::Graph;
using MSA2DAG::matrix_t;

void print_help();
void print_help() {
  cerr << "Ussage: msa2dag [OPTIONS] <input_file> (<input_file_2> ... )" << endl;  // NOLINT
  cerr << endl;
  cerr << "Options:" << endl;
  cerr << "-t --threads=(number of threads)" << endl;
	cerr << "-G prints in  GFA format" << endl;
  cerr << "--help " << endl;
}

int main(int argc, char *argv[]) {
	int c;
	int max_memory_MB = 0;
	int n_threads = 1;
	bool GFA = false;
	while ((c = getopt (argc, argv, "t:m:G")) != -1) {
		switch(c) {
			case 't':
				n_threads = atoi(optarg);
				break;
			case 'm':
				max_memory_MB = atoi(optarg);
				break;
			case 'G':
				GFA = true;
				break;
			default:
				print_help();
				exit(0);
		}
	}
  int rest = argc - optind;
  if (rest < 1 ) {
    print_help();
		exit(EXIT_FAILURE);
	}
	
	//vector<string> M;
	matrix_t M;
	M = MSA2DAG::GraphUtils::ReadMatrixFromFiles(rest, argv+optind);


	Graph graph;
	graph.BuildFromMatrix(M);
	//Graph graph = MSA2DAG::GraphUtils::ParallelBuildFromMatrix(M, n_threads);
	graph.PrintSomeInfo();
	if (!GFA) {
		string output_name = string(argv[optind]) + string(".gv");
		graph.PrintToDot(output_name);
	} else {
		string output_name = string(argv[optind]) + string(".gfa");
		graph.PrintToGFA(output_name);
	}
	cerr << "Success." << endl;
}
