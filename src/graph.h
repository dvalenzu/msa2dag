#ifndef GRAPH_HPP_ 
#define GRAPH_HPP_ 

#include <ostream>
#include <string>
#include <vector>
#include <msa2dag/common.h>

namespace MSA2DAG { 

	// placeholder.
	class Graph {
		public:
			Graph();
			void ValidateMatrix(matrix_t const &M);
			void TestEquallity(Graph const &G);
			void BuildFromMatrix(matrix_t const &M);
			void BuildFromMatrixRange(matrix_t const &M, size_t first_col, size_t last_col);
			void Init(size_t n_rows);
			void AppendMatrixRange(matrix_t const &M);
			void AppendMatrixRange(matrix_t const &M, size_t first_col, size_t last_col);
			void CompleteConstruction();
			size_t AddNode(char label);
			bool HasNeighborByID(size_t id_src, size_t id_target);
			void AddEdge(size_t id_src, size_t id_target);
			void AddEdgeSafely(size_t id_src, size_t id_target);
			bool HasOneOutgoing(size_t id);
			bool HasOneIncoming(size_t id);
			void ValidateFirstWave(std::vector<size_t> const &wave);
			void SpecialMerge(Graph const &G);
			void SquashUnaryPaths();
			void NotSquash();
			void PrintSomeInfo();
			void PrintToDot (std::string const &filename);
			void PrintToDot (std::ostream &os);
			void PrintToGFA (std::string const &filename);
			void PrintToGFA (std::ostream &os);
		private:
			size_t n_nodes;
			size_t n_rows;
			size_t id_s;
			std::vector<char> labels;
			std::vector<std::vector<size_t>> outgoing; 
			//std::vector<std::vector<size_t>> incoming;
			// To be used for merging:
			std::vector<size_t> front_wave;
			std::vector<size_t> back_wave;
			// currently, just before printing.
			//std::vector<bool> to_skip;
			//std::vector<std::string> long_labels;
	};
}
#endif /* GRAPH_HPP_*/
